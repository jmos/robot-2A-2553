/*
 * moteurs.h
 *
 *  Created on: 19 janv. 2018
 *      Author: j.mosnino.15
 */

#ifndef MOTEURS_H_
#define MOTEURS_H_

void init_moteurs(void);

void moteurs_puissance(int pourcentageA, int pourcentageB);

void moteurs_vers_avant(void);

void moteurs_vers_droite(void);

void moteurs_vers_gauche(void);

void moteurs_vers_arriere(void);

void moteurs_rouler(void);

void moteurs_stop(void);

void moteurs_tourner_10_gauche(void);

void moteurs_tourner_10_droite(void);

void moteurs_tourner_90_gauche(unsigned char avec_interrupt);

void moteurs_tourner_90_droite(unsigned char avec_interrupt);

void moteurs_tourner_180(void);



#endif /* MOTEURS_H_ */
