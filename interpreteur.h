/*
 * interpreteur.h
 *
 *  Created on: 6 mars 2018
 *      Author: j.mosnino.15
 */

#ifndef BUS_TP3_2231_INTERPRETEUR_H_
#define BUS_TP3_2231_INTERPRETEUR_H_

void init_UART(void);

void TXdata(unsigned char c);

void affiche_aide(void);

void affiche_erreur(void);

void affiche_separation(void);

void vider_chaine(unsigned char chaine[]);

void analyse_commande(unsigned char chaine[]);

#endif /* BUS_TP3_2231_INTERPRETEUR_H_ */
