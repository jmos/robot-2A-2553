#include <msp430.h>
#include "interpreteur.h"
#include "ADC.h"
#include "mode_auto.h"
#include "moteurs.h"
#include "opto.h"
#include "SPI.h"
#include "UART.h"

/*
 * main.c
 *
 */

/*	DECLARATION DES FONCTIONS ADC + MODE + LED	*/
void init_ADC_ligne4(void);
void init_bouton_mode_auto(void);
void init_LED(void);

/*	DECLARATION DEFINE POUR CHOIX MODE	*/
#define MODE_AUTO 	1
#define MODE_MANUEL 0

/*	DECLARATION DEFINE CARACTERE UART  */
#define SUPP 	0x08
#define ENTREE	0x0D

/*	VARIABLE GLOBALE	*/
unsigned char mode = 0;				/*	enregistre le choix du mode */

unsigned char chaine[10];			/* enregistre la chaine re�ue par UART */
unsigned char *p_chaine = chaine;	/* pointeur sur la chaine */

/*  ----------------------------------------------------
 *	Fonction : main
 *	----------------------------------------------------
 *	Lance les initialisations et g�re le mode 
 *		entr�es	: void
 *		sorties : void
 *	----------------------------------------------------
 */
void main(void) {
	WDTCTL = WDTPW | WDTHOLD;	/* Stop watchdog timer */
	BCSCTL1 = CALBC1_1MHZ;      /* Set DCO to 1Mhz */
	DCOCTL = CALDCO_1MHZ;       /* Set DCO to 1Mhz */

	init_UART();
	init_SPI();
	init_ADC();
	init_ADC_ligne4();
	init_timer_1s();
	init_moteurs();
	init_opto();
	init_bouton_mode_auto();
	init_LED();
	mode = MODE_MANUEL;

	__enable_interrupt();

	while (1) {
		if(mode == MODE_AUTO){
			gestion_mode_auto();
		}
	}

}

/*  ----------------------------------------------------
 *	Fonction : init_LED
 *	----------------------------------------------------
 *	Initialisation de la LED. Elle est allum�e quand le
 *	mode est automatique, �teinte en mode manuel 
 *		entr�es	: void
 *		sorties : void
 *	----------------------------------------------------
 */
void init_LED(void) {
	P1DIR |= BIT0; 		/* P1.0 en sortie */
	P1OUT &= ~BIT0;
}

/*  ----------------------------------------------------
 *	Fonction : init_bouton_mode_auto
 *	----------------------------------------------------
 *	Initialse le bouton S2 de la LaunchPad.
 *	Active �galement une pull-up sur la ligne 
 *		entr�es	: void
 *		sorties : void
 *	----------------------------------------------------
 */
void init_bouton_mode_auto(void) {
	P1SEL &= ~BIT3;
	P1SEL2 &= ~BIT3;

	P1DIR &= ~BIT3;
	P1REN |= BIT3;
	P1OUT |= BIT3;

	P1IES |= BIT3;
	P1IE |= BIT3;
}

/*  ----------------------------------------------------
 *	Fonction : init_ADC_ligne4
 *	----------------------------------------------------
 *	Initialisation de la ligne qui permet la lecture
 *	du capteur IR
 *		entr�es	: void
 *		sorties : void
 *	----------------------------------------------------
 */
void init_ADC_ligne4(void) {
	P1SEL &= ~BIT4;
	P1SEL2 &= ~BIT4;
	P1DIR &= ~BIT4;
}

/*  ----------------------------------------------------
 *	Fonction : USCI0RX_ISR
 *	----------------------------------------------------
 *	Fonction d'interruption sur la r�ception ou l'�mission 
 *	en UART
 *		entr�es	: void
 *		sorties : void
 *	----------------------------------------------------
 */
#pragma vector=USCIAB0RX_VECTOR
__interrupt void USCI0RX_ISR(void) {

	if (UCA0RXBUF & IFG2) {		/* caractere recu */
		mode = MODE_MANUEL;
		P1OUT &= ~BIT0;			/* on eteint la LED */

		unsigned char c;
		c = UCA0RXBUF;

		switch (c) {
		case SUPP:
			p_chaine--;
			break;
		case ENTREE:
			*p_chaine = '\0';
			analyse_commande(chaine);
			p_chaine = chaine;	/* on remet le pointeur sur le d�but de la chaine */
			break;
		default:
			UART_TXdata(c);		/* echo du caractere */
			*p_chaine = c;
			p_chaine++;
			break;
		}
	}
}

/*  ----------------------------------------------------
 *	Fonction : Port_1
 *	----------------------------------------------------
 *	Fonction d'interruption sur le port 1
 *		entr�es	: void
 *		sorties : void
 *	----------------------------------------------------
 */
#pragma vector=PORT1_VECTOR
__interrupt void Port_1(void) {
	if ((P1IFG & BIT3) == BIT3) {	/* on s'assure que l'interruption provient du BIT3 */
		P1OUT |= BIT0;				/* on allume LED pour savoir quel mode */
		mode = MODE_AUTO;
		P1IFG &= ~(BIT3);
	}
}
