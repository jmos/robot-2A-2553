#include <msp430g2553.h>
#include "ADC.h"
#include "moteurs.h"
#include "mode_auto.h"
#include "interpreteur.h"
#include "SPI.h"

/*
 * mode_auto.c
 *
 *  Created on: 11 mars 2018
 *      Author: Joachim
 */

/*  DECLARATION DEFINE POUR LECTURE IR  */
#define SEUIL_OBSTACLE	750
#define SEUIL_VIDE		400

/* DECLARATION DEFINE POUR ENVOIE SPI  */
#define DEVANT      0xB1
#define GAUCHE      0xA1
#define DROITE      0xC1

/* DECLARATION DEFINE POUR TOURNER  AVEC INTERRUPT SUR OPTO */
/* voir commentaires #define dans interpreteur.c */
#define AVEC_INTERRUPT 	0


/*  ----------------------------------------------------
 *  Fonction : gestion_mode_auto
 *  ----------------------------------------------------
 *  Gère la navigation autonome 
 *      entrées : void
 *      sorties : void
 *  ----------------------------------------------------
 */
void gestion_mode_auto(void)
{

    if (lecture_IR() > SEUIL_OBSTACLE) {	/* obstacle devant */
        moteurs_stop();
        SPI_envoie(GAUCHE);
        attente_timer();					/* attente positonnement servo */

        if (lecture_IR() < SEUIL_VIDE) {	/* on essaie d'aller a gauche */
            SPI_envoie(DEVANT);				/* pour repositionner le servo vers l'avant */
            moteurs_tourner_90_gauche(AVEC_INTERRUPT);
        } else {
            SPI_envoie(DROITE);
            attente_timer();				/* attente positonnement servo */

            if (lecture_IR() < SEUIL_VIDE) {/* on essaie d'aller a droite */
                SPI_envoie(DEVANT);
                moteurs_tourner_90_droite(AVEC_INTERRUPT);
            } else {	                    /* obstacle partout : on fait 1/2 tour */
                SPI_envoie(DEVANT);
                moteurs_tourner_180();
            }
        }
    } else { /* on continue tout droit */
        moteurs_vers_avant();
        moteurs_rouler();
    }
}


/*  ----------------------------------------------------
 *  Fonction : init_timer_1s
 *  ----------------------------------------------------
 *  Permet l'initialisation du timer
 *  SMCLK + Prédiv /8 + UP/DOWN 
 *      entrées : void
 *      sorties : void
 *  ----------------------------------------------------
 */
void init_timer_1s(void)
{
    TA0CTL = TASSEL_2 | ID_3 | MC_3;
    TA0CCR0 = 0xFFFF;
}

/*  ----------------------------------------------------
 *  Fonction : attente_timer
 *  ----------------------------------------------------
 *  Permet une temporisation d'1 seconde
 *  /!\ une fois dans la fonction impossible d'en sortir
 *      entrées : void
 *      sorties : void
 *  ----------------------------------------------------
 */
void attente_timer(void)
{
    TA0R = 0x0000;              /* on remet le compteur à 0 */
    TA0CTL &= ~TAIFG;			/* s'il s'est deroule plus d'1s entre 2 attentes, */
    							/* le flag est deja passe a 1 */
    while (!(TA0CTL & TAIFG))
        ;                       /* attente que TA0R = TA0CCR0 */
    TA0CTL &= ~TAIFG;
}

/*  ----------------------------------------------------
 *  Fonction : lecture_IR
 *  ----------------------------------------------------
 *  Permet de lire la valeur convertie du capteur IR
 *      entrées : void
 *      sorties : void
 *  ----------------------------------------------------
 */
int lecture_IR(void)
{
    ADC_demarrer_conversion(4);

    return ADC_lire_resultat();
}

