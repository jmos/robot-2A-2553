/*
 * interpreteur.c
 *
 *  Created on: 6 mars 2018
 *  Author: j.mosnino.15
 *
 *	Fichier contenant les fonctions permettant de traiter la commande transmise
 *
 *	P1.1|-- RX data in
 *	P1.2|-- TX data out
 */

#include <msp430g2553.h>
#include "interpreteur.h"
#include "UART.h"
#include "ADC.h"
#include "moteurs.h"

/* DECLARATION DEFINE CARACTERE RECU PAR UART */
#define AVANCER		'o'
#define RECULER		'l'
#define STOP 		's'
#define DROITE		'm'
#define GAUCHE		'k'
#define AIDE		'h'

/* DECLARATION DEFINE POUR TOURNER  SANS INTERRUPT SUR OPTO */
/* puisque fonction analyse_commande est appellee par une interruption sur l'UART, */
/* on ne peut reattendre des interruptions sur les opto sinon bug */
#define SANS_INTERRUPT 	0

/* VARIABLES DEFINISSANT SI LE ROBOT ROULE + AVANT */
unsigned char en_marche = 0;
unsigned char en_avant = 0;

/*  ----------------------------------------------------
 *	Fonction : analyse_commande
 *	----------------------------------------------------
 *	Traite la commande reçue par UART 
 *		entrées	:
 *			unsigned char chaine[] : la chaine contenant la commande
 *		sorties : void
 *	----------------------------------------------------
 */
void analyse_commande(unsigned char chaine[]) {
	unsigned char taille = 0; /* la taille de la commande */

	UART_TXdata('>');

	while (chaine[taille] != 0x00)
		taille++;

	if (taille > 1) {
		affiche_chaine(
				"Cet analyseur ne prend que des commandes d'un caractere");
	} else {
		switch (chaine[0]) {
		case AVANCER:
			en_marche = 1;
			en_avant = 1;
			moteurs_vers_avant();
			moteurs_rouler();
			break;
		case STOP:
			en_marche = 0;
			moteurs_stop();
			break;
		case DROITE:
			if (en_marche == 1) {
				moteurs_tourner_10_droite();
				if(en_avant == 1){
	                moteurs_vers_avant();
				} else {
				    moteurs_vers_arriere();
				}
				moteurs_rouler();
			} else {
				moteurs_tourner_90_droite(SANS_INTERRUPT);
			}
			break;
		case GAUCHE:
			if (en_marche == 1) {
				moteurs_tourner_10_gauche();
				if(en_avant == 1) {
				    moteurs_vers_avant();
				} else {
				    moteurs_vers_arriere();
				}
				moteurs_rouler();
			} else {
				moteurs_tourner_90_gauche(SANS_INTERRUPT);
			}
			break;
		case RECULER:
		    en_marche = 1;
		    en_avant = 0;
		    moteurs_stop();
		    moteurs_vers_arriere();
		    moteurs_rouler();
		    break;
		case AIDE:
			affiche_aide();
			break;
		default: /* Commande non reconnu */
			affiche_erreur();
			break;
		}

	}

	affiche_separation();
	vider_chaine(chaine);
}

/*  ----------------------------------------------------
 *	Fonction : affiche_aide
 *	----------------------------------------------------
 *	Transmet par UART une aide affichant les commandes dispo  
 *		entrées	: void
 *		sorties : void
 *	----------------------------------------------------
 */
void affiche_aide(void) {
	affiche_chaine("Les commandes disponibles sont :");
	retour_ligne();
	affiche_chaine("h --> affiche aide");
	retour_ligne();
	affiche_chaine("o --> avancer");
	retour_ligne();
	affiche_chaine("l --> reculer");
	retour_ligne();
	affiche_chaine("m --> tourner droite");
	retour_ligne();
	affiche_chaine("k --> tourner gauche");
    retour_ligne();
    affiche_chaine("s --> arreter");
}

/*  ----------------------------------------------------
 *	Fonction : affiche_aide
 *	----------------------------------------------------
 *	Transmet par UART un message d'erreur quand la commande
 *	est non reconnue  
 *		entrées	: void
 *		sorties : void
 *	----------------------------------------------------
 */
void affiche_erreur(void) {
	unsigned char erreur[] = "Commande non reconnue. ";

	affiche_chaine(erreur);
	affiche_aide();
}

/*  ----------------------------------------------------
 *	Fonction : vider_chaine
 *	----------------------------------------------------
 *	Vide une chaine de caractère 
 *		entrées	:
 *			unsigned char chaine[] : la chaine à vider
 *		sorties : void
 *	----------------------------------------------------
 */
void vider_chaine(unsigned char chaine[]) {
	int i = 0;
	while (chaine[i] != '\0') {
		chaine[i] = '\0';
		i++;
	}
}

/*  ----------------------------------------------------
 *	Fonction : affiche_separation
 *	----------------------------------------------------
 *	Transmet en UART une séparation
 *		entrées	: void
 *		sorties : void
 *	----------------------------------------------------
 */
void affiche_separation(void) {
    retour_ligne();
	affiche_chaine("*****");
	retour_ligne();
}
