/*
 * UART.c
 *
 *  Created on: 16 mars 2018
 *      Author: j.mosnino.15
 */

#include <msp430g2253.h>


/*  ----------------------------------------------------
 *	Fonction : init_UART
 *	----------------------------------------------------
 *	Permet l'initialisation de l'UART
 *		entrées	: void
 *		sorties : void
 *	----------------------------------------------------
 */
void init_UART(void) {
	P1SEL |= (BIT1 + BIT2); /* P1.1 = RX, P1.2=TX */
	P1SEL2 |= (BIT1 + BIT2);
	UCA0CTL1 |= UCSSEL_2; /* SMCLK */
	UCA0BR0 = 104; /* 1MHz / 104 = 9600*/
	UCA0BR1 = 0;
	UCA0CTL0 &= ~UCPEN & ~UCPAR & ~UCMSB;
	UCA0CTL0 &= ~UC7BIT & ~UCSPB & ~UCMODE1;
	UCA0CTL0 &= ~UCMODE0 & ~UCSYNC;
	UCA0CTL1 &= ~UCSWRST; /* Initialize USCI state machine */
	IE2 |= UCA0RXIE; /* Interruptions sur USCI_A0 RX (récéption) activée */
}

/*  ----------------------------------------------------
 *	Fonction : UART_TXdata
 *	----------------------------------------------------
 *	Envoie un caractère par UART sur la broche TX
 *		entrées	:
 *			unsigned char c : le caractère à transmettre
 *		sorties : void
 *	----------------------------------------------------
 */
void UART_TXdata(unsigned char c) {
	while (!(IFG2 & UCA0TXIFG))
		; /* Attente USCI_A0 TX buffer prêt */
	UCA0TXBUF = c; /* Le buffer prend le caractère à transmettre */
}

/*  ----------------------------------------------------
 *	Fonction : affiche_chaine
 *	----------------------------------------------------
 *	Transmet une chaine de caractère par UART
 *		entrées	:
 *			unsigned char c[] : la chaine à transmettre
 *		sorties : void
 *	----------------------------------------------------
 */
void affiche_chaine(unsigned char c[]) {
	int i = 0;

	while (c[i] != '\0') {
		UART_TXdata(c[i]);
		i++;
	}
}

/*  ----------------------------------------------------
 *	Fonction : retour_ligne
 *	----------------------------------------------------
 *	Transmet en UART un retour chariot + début ligne
 *		entrées	: void
 *		sorties : void
 *	----------------------------------------------------
 */
void retour_ligne(void) {
    UART_TXdata(0x0B);
    UART_TXdata(0x0D);
}


