#include "msp430.h"
#include "ADC.h"

void init_ADC(void)
{
	  ADC10CTL0 = ADC10CTL1 = 0;

// Choix de la r�f�rence de tension Vcc GND
// R�f�rence interne active et g�n�rateur � 2,5 Volts  ADC10 actif
// Les autres bits sont suppos�s � 0 

	  ADC10CTL0 =  SREF_0 + ADC10SHT_0  + REF2_5V + REFON + ADC10ON;  ;  

// Choix du diviseur par 1 pour l'horloge, d�marrage conversion logiciel
// Horloge de conversion 1MHz, conversion monovoie-monocoup	

	  ADC10CTL1 =  ADC10DIV_0 + ADC10SSEL_2 +  SHS_0 + CONSEQ_0 ;

}



void ADC_demarrer_conversion(unsigned char voie)
{
     ADC10CTL1 = (voie * 0x1000)+ ADC10DIV_0 + ADC10SSEL_2 +  SHS_0 + CONSEQ_0 ;
     ADC10CTL0 |= ENC + ADC10SC;     // Sampling and conversion start
 }  


int ADC_lire_resultat ()
{
  	while (ADC10CTL1 & ADC10BUSY);	// Tant que ADC occup� on attend
	ADC10CTL0 &= ~ENC;		// Conversion finie alors Disable ADC conversion

    return ADC10MEM;	        // Return Conversion value
}


/*...................................................................................*/
/* void convert_ASCII(int c, buffer)                                                 */
/*...................................................................................*/
/* Description : Convert the value of an int in ASCII                                */
/* Inputs :                                                                          */
/*      - int c: value of the number you want to convert  (unsigned)                 */
/*      - buffer : adress of the ASCII buffer to put the results                     */
/* Output : void                													 */
/*...................................................................................*/

void convert_ASCII(int c, unsigned char hexa[4]) {

	int digit1, digit2, digit3, digit4;

	digit4 = c & (0x000F);                    //  d�calage de bit selon le digit
	digit3 = (c & (0x00F0)) >> 0x04;
	digit2 = (c & (0x0F00)) >> 0x08;
	digit1 = (c & (0xF000)) >> 0x0C;
	hexa[3] = convert_digit(digit4); // placement dans un tableau de 4 caract�res + conversion
	hexa[2] = convert_digit(digit3);
	hexa[1] = convert_digit(digit2);
	hexa[0] = convert_digit(digit1);

}

/*...................................................................................*/
/* void convert_digit(int value)                                                      */
/*...................................................................................*/
/* Description : Use for an other fonction                                           */
/* Inputs :                                                                          */
/*      - int value: value of the digit you want to convert in ASCII                       */
/*                                                                                   */
/* Output : int  : value of the ASCII code of the digit                                                                    */
/*...................................................................................*/

int convert_digit(int value)            // fonction de conversion en hexadecimal
{
	int valTens = 0;

	if (value <= 9)
		valTens = value + 0x30;          // pour les unit�s

	else
		valTens = value + 0x37;                  // pour les nombres > 9

	return valTens;
}

/*...................................................................................*/
/* void convert_Hex_Dec(int valeur_hex)                                            */
/*...................................................................................*/
/* Description : Convert an hexadecimal to decimal                                   */
/* Inputs :                                                                          */
/*      - char toExtract[]: value of the number in hexadecimal you want to convert   */
/*                                                                                   */
/* Output : char                                                                     */
/*...................................................................................*/
int convert_Hex_Dec(int valeur_hex) {
	unsigned char dec[4];
	int val_dec;
	dec[0] = valeur_hex / 0x03e8;
	dec[1] = (valeur_hex % 0x03e8) / 0x64;
	dec[2] = (valeur_hex % 0x64) / 0x0A;
	dec[3] = (valeur_hex % 0x0A);
	val_dec = (dec[0] << 12) + (dec[1] << 8) + (dec[2] << 4) + dec[3];
	return val_dec;

}
