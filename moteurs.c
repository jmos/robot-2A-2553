#include <msp430g2553.h>
#include "moteurs.h"
#include "opto.h"

/*  VARRIABLES GLOBALES DECLAREES DANS opto.c */
extern unsigned char opto_droite;
extern unsigned char opto_gauche;

/*  ----------------------------------------------------
 *  Fonction : init_moteurs
 *  ----------------------------------------------------
 *  Permet l'initalisation des moteurs ainsi que du PWM :
 *  SMCLK, UP, Pr�div /1
 *  La sortie des pwm est de type RESET / SET
 *      entr�es : void
 *      sorties : void
 *  ----------------------------------------------------
 */
void init_moteurs(void) {
	P2SEL &= ~(BIT1 | BIT5);
	P2SEL |= (BIT2 | BIT4);
	P2SEL2 &= ~(BIT1 | BIT2 | BIT4 | BIT5);

	P2DIR |= (BIT1 | BIT2 | BIT4 | BIT5);

	TA1CTL |= (TASSEL_2 | MC_1 | ID_0);

	TA1CCTL1 |= OUTMOD_7; //moteur gauche
	TA1CCTL2 |= OUTMOD_7; //moteur droite
}

/*  ----------------------------------------------------
 *  Fonction : moteurs_puissance
 *  ----------------------------------------------------
 *  Permet de d�finir la p�riode du PWM ainsi que son
 *  rapport cyclcique
 *      entr�es : 
 *          int pourcentageGauche : le % de puissance par rapport � vitesse max (de 0 � 100)
 *          int pourcentageDroite : le % de puissance par rapport � vitesse max (de 0 � 100)
 *      sorties : void
 *  ----------------------------------------------------
 */
void moteurs_puissance(int pourcentageGauche, int pourcentageDroite) {
	TA1CCR0 = 240; //4.16kHz
	TA1CCR1 = (int) (240 * pourcentageGauche) / 100;
	TA1CCR2 = (int) (240 * pourcentageDroite) / 100;
}

void moteurs_stop(void) {
	moteurs_puissance(0, 0);
}

void moteurs_rouler(void) {
	moteurs_puissance(50, 50);
}

void moteurs_vers_avant(void) {
	P2OUT |= (BIT5);
	P2OUT &= ~BIT1;
}

void moteurs_vers_droite(void) {
	P2OUT &= ~BIT5;
	P2OUT &= ~BIT1;
}

void moteurs_vers_gauche(void) {
	P2OUT |= BIT5;
	P2OUT |= BIT1;
}

void moteurs_vers_arriere(void) {
	P2OUT &= ~BIT5;
	P2OUT |= BIT1;
}

void moteurs_tourner_10_gauche(void) {
    moteurs_vers_gauche();
    moteurs_puissance(40, 40);
    __delay_cycles(200000);
    moteurs_stop();
}

void moteurs_tourner_10_droite(void) {
    moteurs_vers_droite();
    moteurs_puissance(40, 40);
    __delay_cycles(200000);
    moteurs_stop();
}

void moteurs_tourner_90_gauche(unsigned char avec_interrupt) {
    moteurs_vers_gauche();
    moteurs_puissance(40, 40);
    if(avec_interrupt == 1) {
    	opto_droite = 0;
    	opto_gauche = 0;
    	while((opto_droite < 7) && (opto_gauche < 7))
    		;
    } else {
    	__delay_cycles(800000);
    }
	moteurs_stop();

}

void moteurs_tourner_90_droite(unsigned char avec_interrupt) {
    moteurs_vers_droite();
    moteurs_puissance(40, 40);
    if(avec_interrupt == 1) {
    	opto_droite = 0;
    	opto_gauche = 0;
        while((opto_droite < 7) && (opto_gauche < 7))
        	;
    } else {
    	__delay_cycles(800000);
    }
    moteurs_stop();
}

void moteurs_tourner_180(void) {
    moteurs_tourner_90_droite(1);
    moteurs_tourner_90_droite(1);
}




