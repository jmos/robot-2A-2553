/*
 * opto.c
 *
 *  Created on: 11 mars 2018
 *      Author: Joachim
 */

#include <msp430g2533.h>
#include "opto.h"

/*  DECLARATION VARIABLES GLOBALES QUI COMPTE LES INTERRUPTIONS SUR LES OPTO  */	
unsigned char opto_droite;
unsigned char opto_gauche;

/*  ----------------------------------------------------
 *	Fonction : init_opto
 *	----------------------------------------------------
 *	Initialisation des opto coupleurs en activant les
 *	interruptions 
 *		entrées	: void
 *		sorties : void
 *	----------------------------------------------------
 */
void init_opto(void) {
	P2SEL &= ~(BIT0 | BIT3);
	P2SEL2 &= ~(BIT0 | BIT3);

	P2DIR &= ~(BIT0 | BIT3);

	P2IFG &= ~(BIT0 | BIT3);
	P2IES &= ~(BIT0 | BIT3);
	P2IE |= (BIT0 | BIT3);
}

/*  ----------------------------------------------------
 *	Fonction : Port_2()
 *	----------------------------------------------------
 *	Fonction d'interruption du port 2
 *		entrées	: void
 *		sorties : void
 *	----------------------------------------------------
 */
#pragma vector=PORT2_VECTOR
__interrupt void Port_2(void) {
	if ((P2IFG & BIT3) == BIT3) {	/* interruption a lieu sur P2.3 */
		opto_droite++;
		P2IFG &= ~(BIT3);
	}
	if ((P2IFG & BIT0) == BIT0) {	/* interruption a lieu sur P2.0 */
		opto_gauche++;
		P2IFG &= ~(BIT0);
	}
}

