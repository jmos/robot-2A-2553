/*
 * mode_auto.h
 *
 *  Created on: 11 mars 2018
 *      Author: Joachim
 */

#ifndef MODE_AUTO_H_
#define MODE_AUTO_H_

void init_timer_1s(void);

void attente_timer(void);

int lecture_IR(void);

void gestion_mode_auto(void);

void SPI_envoie(unsigned char data);


#endif /* MODE_AUTO_H_ */
