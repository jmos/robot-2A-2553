


void init_ADC(void);

void ADC_demarrer_conversion(unsigned char voie);

int ADC_lire_resultat ();

int convert_digit(int value);

void convert_ASCII(int c, unsigned char hexa[4]);

int convert_Hex_Dec(int valeur_hex);
