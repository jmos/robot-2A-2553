/*
 * UART.h
 *
 *  Created on: 16 mars 2018
 *      Author: j.mosnino.15
 */

#ifndef UART_H_
#define UART_H_


void init_UART(void);

void UART_TXdata(unsigned char c);

void affiche_chaine(unsigned char c[]);

void retour_ligne(void);


#endif /* UART_H_ */
