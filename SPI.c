/*
 * SPI.c
 *
 *  Created on: 16 mars 2018
 *
 *      Author: j.mosnino.15
 *
 * 	P1.5|-> Serial Clock Out
 * 	P1.7|-> Data Out
 */

#include <msp430g2553.h>
#include "SPI.h"

/*  ----------------------------------------------------
 *	Fonction : init_spi
 *	----------------------------------------------------
 *	Initialisation de la communication SPI
 *		entrées	: void
 *		sorties : void
 *	----------------------------------------------------
 */
void init_SPI(void) {
	P1DIR |= BIT5;									/* BIT5 : Horloge */
	P1SEL |= BIT5 + BIT7;							/* BIT7 : Donnees envoyees */
	P1SEL2 |= BIT5 + BIT7;
	UCB0CTL0 |= UCCKPL + UCMSB + UCMST + UCSYNC;  	/* 8-bits SPI master */
	UCB0CTL1 |= UCSSEL_2;                     		/* SMCLK */
	UCB0BR0 |= 0x02;                          		/* Horloge /2 = 500kHz */
	UCB0BR1 = 0;                           
	UCB0CTL1 &= ~UCSWRST;                   		/*Initialise USCI */

	__delay_cycles(75);                 			/* Attente initialisation slave */
}

/*  ----------------------------------------------------
 *	Fonction : SPI_envoie
 *	----------------------------------------------------
 *	Permet d'envoyer un caractere par SPI
 *		entrées	:
 *			unsigned char data : le caractere a transmettre
 *		sorties : void
 *	----------------------------------------------------
 */
void SPI_envoie(unsigned char data) {
    while (!(IFG2 & UCB0TXIFG))
        ;	/* Attente USCI_B0 TX buffer prêt */
    UCB0TXBUF = data;
}
